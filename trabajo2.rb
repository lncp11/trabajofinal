
TAMANIO_DE_FILA = 8
TAMANIO_DEL_HOSPITAL = TAMANIO_DE_FILA * TAMANIO_DE_FILA
HABITACION_INFECTADA = 'X'.freeze
HABITACION_LIMPIA = ' '.freeze
MUERTES_POR_HABITACION = 10
FACTOR_PORCENTAJE = 1.5625

def generar_habitaciones(caracter = ' ')
  caracter * TAMANIO_DEL_HOSPITAL
end

def calcula_muertes(habitaciones)
  contar_habitaciones_por_cadena(habitaciones)
end

def calcula_muertes_por_ciclo(hab,muertos_anteriores)
  posiciones_actuales = []
  hab.each_char.with_index do |caracter, idx|
    posiciones_actuales << idx if caracter == HABITACION_INFECTADA
  end
  muertos_anteriores.each do |posicion|
    hab[posicion] = HABITACION_LIMPIA
  end
  c = contar_habitaciones_por_cadena(hab)
  hab.each_char.with_index do |caracter, idx|
    muertos_anteriores << idx if caracter == HABITACION_INFECTADA
  end
  posiciones_actuales.each do |posi|
    hab[posi] = HABITACION_INFECTADA
  end
  return c
end


def decidir_infeccion(habitaciones_infectadas = 0, es_habitacion_infectada = false)
  hi = habitaciones_infectadas
  es_habitacion_infectada ? infecta_habitacion(hi) : desinfecta_habitacion(hi)
end

def infecta_habitacion(habitaciones_infectadas = 0)
  hi = habitaciones_infectadas
  if [0, 1].include?(hi)
    HABITACION_LIMPIA
  elsif [2, 3].include?(hi)
    HABITACION_INFECTADA
  else
    HABITACION_LIMPIA
  end
end

def desinfecta_habitacion(habitaciones_infectadas = 0)
  hi = habitaciones_infectadas
  if [0, 1, 2].include?(hi)
    HABITACION_LIMPIA
  elsif hi == 3
    HABITACION_INFECTADA
  else
    HABITACION_LIMPIA
  end
end

def imprimir_hospital(hospital)
  h = hospital
  print "\n MEMORIAL HOSPITAL \n "
  print ""+" 1"+" "+"2"+" "+"3"+" "+"4"+" "+"5"+" "+"6"+" "+"7"+" "+"8"+" "+"\n"
  print "1 "+h[0]+" "+h[1]+" "+h[2]+" "+h[3]+" "+h[4]+" "+h[5]+" "+h[6]+" "+h[7]+"\n"
  print "2 "+h[8]+" "+h[9]+" "+h[10]+" "+h[11]+" "+h[12]+" "+h[13]+" "+h[14]+" "+h[15]+"\n"
  print "3 "+h[16]+" "+h[17]+" "+h[18]+" "+h[19]+" "+h[20]+" "+h[21]+" "+h[22]+" "+h[23]+"\n"
  print "4 "+h[24]+" "+h[25]+" "+h[26]+" "+h[27]+" "+h[28]+" "+h[29]+" "+h[30]+" "+h[31]+"\n"
  print "5 "+h[32]+" "+h[33]+" "+h[34]+" "+h[35]+" "+h[36]+" "+h[37]+" "+h[38]+" "+h[39]+"\n"
  print "6 "+h[40]+" "+h[41]+" "+h[42]+" "+h[43]+" "+h[44]+" "+h[45]+" "+h[46]+" "+h[47]+"\n"
  print "7 "+h[48]+" "+h[49]+" "+h[50]+" "+h[51]+" "+h[52]+" "+h[53]+" "+h[54]+" "+h[55]+"\n"
  print "8 "+h[56]+" "+h[57]+" "+h[58]+" "+h[59]+" "+h[60]+" "+h[61]+" "+h[62]+" "+h[63]+"\n"
end

def imprimir_estado_de_la_bacteria(estado = '', line = '-', line_width = 52)
  puts "#{line * line_width}\nBACTERIA #{estado}\n#{line * line_width}"
end

def ejecutar_desinfeccion_total(habitaciones)
  muertos_totales = 0
  contador_de_ciclo = 1
  muertos = []
  loop do
    imprimir_hospital(habitaciones)
    habitaciones_infectadas = calcula_muertes(habitaciones)
    muertes_por_ciclo = calcula_muertes_por_ciclo(habitaciones,muertos)
    muertos_totales += (muertes_por_ciclo * MUERTES_POR_HABITACION)
    puts "\n Ciclo #{contador_de_ciclo}\t \t Muertos en el ciclo : #{muertes_por_ciclo * MUERTES_POR_HABITACION}"
    puts "Habitaciones Infectadas : #{habitaciones_infectadas}"
    puts "Porcentaje de Infección :  #{habitaciones_infectadas * FACTOR_PORCENTAJE}%"
    puts "Muertos Totales :  #{muertos_totales}"
    nuevas_bacterias = revisar_infeccion_del_hospital(habitaciones)
    contador_de_ciclo += 1
    if habitaciones == generar_habitaciones
      imprimir_estado_de_la_bacteria('DESAPARECE')
      break
    elsif habitaciones == nuevas_bacterias
      imprimir_estado_de_la_bacteria('SE MANTIENE')
      break
    end
    habitaciones = nuevas_bacterias
  end
  puts '-----------------------------------------------------'
  puts "#{contador_de_ciclo - 1} CICLOS POSIBLES PARA SIMULACION DE DESAPARICIÓN DE LA BACTERIA"
  puts '-----------------------------------------------------'
end

def ejecutar_infeccion_total(habitaciones)
  muertos_totales = 0
  contador_de_ciclo = 1
  muertos = []
  loop do
    imprimir_hospital(habitaciones)
    habitaciones_infectadas = calcula_muertes(habitaciones)
    muertes_por_ciclo = calcula_muertes_por_ciclo(habitaciones,muertos)
    muertos_totales += (muertes_por_ciclo * MUERTES_POR_HABITACION)
    puts "\n Ciclo #{contador_de_ciclo}\t \t Muertos en el ciclo : #{muertes_por_ciclo * MUERTES_POR_HABITACION}"
    puts "Habitaciones Infectadas : #{habitaciones_infectadas}"
    puts "Porcentaje de Infección :  #{habitaciones_infectadas * FACTOR_PORCENTAJE}%"
    puts "Muertos Totales :  #{muertos_totales}"
    nuevas_bacterias = revisar_infeccion_del_hospital(habitaciones)
    contador_de_ciclo += 1
    if habitaciones == generar_habitaciones('X')
      imprimir_estado_de_la_bacteria('INFECTA TODO EL HOSPITAL')
      break
    elsif habitaciones == generar_habitaciones
      imprimir_estado_de_la_bacteria('DESAPARECE')
      break
    elsif habitaciones == nuevas_bacterias
      imprimir_estado_de_la_bacteria('SE MANTIENE')
      break
    end
    habitaciones = nuevas_bacterias
  end
  puts '-----------------------------------------------------'
  puts "#{contador_de_ciclo - 1} CICLOS POSIBLES PARA SIMULACION DE INFECCIÓN TOTAL DEL HOSPITAL"
  puts '-----------------------------------------------------'
end

def buscar_hermanos_de_habitacion(eje_x, eje_y, habitaciones)
  indice_habitacion_actual = (TAMANIO_DE_FILA * eje_x) + eje_y
  habitacion_actual = habitaciones[indice_habitacion_actual]
  limite_izquierda = TAMANIO_DE_FILA * eje_x
  limite_derecha = (eje_x + 1) * (TAMANIO_DEL_HOSPITAL - 1)
  [
    buscar_habitaciones_anteriores(indice_habitacion_actual, limite_izquierda),
    buscar_habitaciones_posteriores(indice_habitacion_actual + 1, limite_derecha),
    es_habitacion_infectada(habitacion_actual)
  ]
end

def buscar_habitaciones_anteriores(indice_habitacion_actual, limite_izquierda)
  habitacion_anterior = indice_habitacion_actual
  ha = habitacion_anterior
  if indice_habitacion_actual - 1 >= limite_izquierda
    ha = indice_habitacion_actual - 1
  elsif ha == limite_izquierda
    ha = limite_izquierda
  end
  ha
end

def buscar_habitaciones_posteriores(indice_habitacion_actual, limite_derecha)
  habitacion_posterior = indice_habitacion_actual
  hp = habitacion_posterior
  if indice_habitacion_actual + 1 <= limite_derecha
    hp = indice_habitacion_actual + 1
  elsif hp - 1 == indice_habitacion_actual
    hp = indice_habitacion_actual
  end
  hp
end

def es_habitacion_infectada(habitacion_actual)
  habitacion_actual == HABITACION_INFECTADA ? true : false
end

def obtener_habitaciones(habitaciones)
  print "\nIngrese cantidad de habitaciones a infectar => "
  gets.chomp.to_i.times do
    coordenada = obtener_coordenada(obtener_pares_binarios)
    habitaciones[coordenada] = HABITACION_INFECTADA
  end
  habitaciones
end

def obtener_pares_binarios
  print 'Ingrese par ordenado de habitacion a infectar, Ejemplo: a,b => '
  gets.chomp.split(',').map(&:to_i)
end

def obtener_coordenada(part_ordenado)
  TAMANIO_DE_FILA * (part_ordenado.first - 1) + part_ordenado.last - 1
end

def ejecutar_ciclos(ciclos, habitaciones)
  muertos_totales = 0
  numero_de_ciclo = 1
  muertos = []
  ciclos.times do
    imprimir_hospital(habitaciones)
    habitaciones_infectadas = calcula_muertes(habitaciones)
    muertes_por_ciclo =  calcula_muertes_por_ciclo(habitaciones,muertos)
    muertos_totales += (muertes_por_ciclo * MUERTES_POR_HABITACION)
    puts "\nCiclo #{numero_de_ciclo}\t \t Muertos en el ciclo : #{muertes_por_ciclo * MUERTES_POR_HABITACION}"
    puts "Habitaciones Infectadas : #{habitaciones_infectadas}"
    puts "Porcentaje de Infección :  #{habitaciones_infectadas * FACTOR_PORCENTAJE}%"
    puts "Muertos Totales :  #{muertos_totales}"
    nuevas_bacterias = revisar_infeccion_del_hospital(habitaciones)
    habitaciones = nuevas_bacterias
    numero_de_ciclo += 1
  end
  puts "\nFIN DE SIMULACION POR CICLOS\n"
end

def contar_habitaciones_por_cadena(string, substring = HABITACION_INFECTADA)
  string.scan(/(?=#{substring})/).count
end

def revisar_infeccion_del_hospital(habitaciones = generar_habitaciones)
  # habitaciones_infectadas = ''
  tamanio = obtener_habitaciones1(habitaciones_infectadas)
  nuevo_habitaciones_infectadas =''
  eje_x=0

  while true
    break if eje_x > tamanio - 1
    columna_actual = tamanio * eje_x
    (0..7).each do |eje_y|
      nuevas_habitaciones_infectadas = ''
      coordenadas_de_habitaciones_hermanas = buscar_hermanos_de_fila1(eje_x, eje_y, habitaciones_infectadas)
      nuevas_habitaciones_infectadas << habitaciones_infectadas[coordenadas_de_habitaciones_hermanas[0]..coordenadas_de_habitaciones_hermanas[1]]
      if columna_actual-1>0
        coordenada_anterior = [coordenadas_de_habitaciones_hermanas[0]-8, coordenadas_de_habitaciones_hermanas[1]-8]
        nuevas_habitaciones_infectadas << habitaciones_infectadas[coordenada_anterior[0]..coordenada_anterior[1]]
      end
      if columna_actual+1 <(tamanio*tamanio)-tamanio
        coordenada_posterior = [coordenadas_de_habitaciones_hermanas[0]+8, coordenadas_de_habitaciones_hermanas[1]+8]
        nuevas_habitaciones_infectadas << habitaciones_infectadas[coordenada_posterior[0]..coordenada_posterior[1]]
      end
      cantidad_hermanos = count_el1(nuevas_habitaciones_infectadas)
      if coordenadas_de_habitaciones_hermanas[2]
        cantidad_hermanos -= 1
      end
      # cantidad_hermanos
      nuevo_habitaciones_infectadas << decidir_infeccion_de_bacteria1(cantidad_hermanos,coordenadas_de_habitaciones_hermanas[2])
    end
    eje_x += 1
  end
  nuevo_habitaciones_infectadas
end

def obtener_habitaciones_actual(coordenadas, habitaciones)
  habitaciones[coordenadas[0]...coordenadas[1]]
end

def obtener_habitaciones_superior(coordenadas, habitaciones)
  habitaciones_izquierda = coordenadas[0] - TAMANIO_DE_FILA
  habitaciones_derecha = coordenadas[1] - TAMANIO_DE_FILA
  habitaciones[habitaciones_izquierda...habitaciones_derecha]
end

def obtener_habitaciones_inferior(coordenadas, habitaciones)
  habitaciones_izquierda = coordenadas[0] + TAMANIO_DE_FILA
  habitaciones_derecha = coordenadas[1] + TAMANIO_DE_FILA
  habitaciones[habitaciones_izquierda...habitaciones_derecha]
end

habitaciones_generadas = generar_habitaciones
imprimir_hospital(habitaciones_generadas)
habitaciones = obtener_habitaciones(habitaciones_generadas)
puts '--------------------------------------------------'
print 'Desea realizar simulación de infección por ciclos, Ejemplo: SI/NO => '
if gets.chomp.casecmp('SI').zero?
  print 'Ingrese número de ciclos a simular => '
  ejecutar_ciclos(gets.chomp.to_i, habitaciones)
else
  print 'Desea simular la desaparición de la bacteria, Ejemplo: SI/NO=> '
  gets.chomp.casecmp('SI').zero? ? ejecutar_desinfeccion_total(habitaciones) : ejecutar_infeccion_total(habitaciones)
end


