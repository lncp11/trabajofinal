inicio
  inicializar constantes iniciales
  se imprime el status del hospital
  se obtiene del usuario la cantidad de habitaciones a infectar
  por la cantidad de habitaciones a infectar se obtiene su par ordenado
  se infecta la habitacion usando su par ordenado
  se obtiene del usuario si desea simular por ciclos
    si es afirmativo
      se obtiene la cantidad de ciclos a simular
        por cada ciclo
          se imprime el status del hospital y se guarda en a
          se calcula las muertes actuales y se guarda en b
          se calcula las muertes por ciclo y se guarda en c
          se calcula las muertes totales y se guarda en d
          se revisa la infeccion del hospital
            se crea una cadena e para guardar el resultado
            se itera cada una de las habitaciones
              se obtiene los elementos adjacentes, superiores e inferiores de cada celda
              se cuenta todas las X de la cadena obtenida
              usando la cantidad de las X se decide si se infecta o no la habitacion
              se guarda el resultado en la cadena e     
          si quedan ciclos
            se vuelve a ejecutar el ciclo
          sino
            fin        
    si no es afirmativo
      se obtiene si desea simular la desaparicion de la bacteria
        si es afirmativo
          se ejecuta desinfeccion total
        si es negativo
          se ejecuta infeccion totala
fin
