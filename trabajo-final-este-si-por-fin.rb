
def generar_habitaciones(str = '')
  habs = 8
  idx = 1
  while true
    break if idx > habs
    str << ' '
    idx += 1
  end
  str*habs
end

def obtener_bacterias(cant_habitaciones)
  print 'cantidad de bacterias => '
  cantidad = gets.chomp.to_i
  habitaciones = obtener_habitaciones(cant_habitaciones)
  (1..cantidad).each do
    pb = obtener_pares_binarios
    coordenada = (habitaciones*(pb[0]-1))+pb[1]
    cant_habitaciones[coordenada-1] = 'X'
  end
  cant_habitaciones
end

def obtener_habitaciones(habitaciones)
  Math.sqrt(habitaciones.length).round
end

def obtener_pares_binarios
  print "Par ordenado de habitacion infectada, Ejemplo: a,b => "
  par_binario = gets
  [ par_binario[0].to_i , par_binario[2].to_i ]
end


def ejecutar_ciclos(ciclos, bacterias)
  suma = 0
  i = 1
  muertos=[]
  (1..ciclos).each do
    imprimir_hospital(bacterias)
    c = calcula_muertes_por_ciclo(bacterias,muertos)
    d = count_el(bacterias)
    suma += (c*10)
    puts "Ciclo " + i.to_s+"\t \t Muertos en el ciclo : " + (c*10).to_s
    puts "Habitaciones Infectadas : " + d.to_s
    puts "Porcentaje de Infección :  " + (d*1.5625).to_s+"%"
    puts "Muertos Totales :  " + suma.to_s
    nuevas_bacterias = revisar_infeccion_del_hospital(bacterias)
    if bacterias == generar_habitaciones('X')
      puts 'BACTERIA INFECTA TODO EL HOSPITAL'
      break
    elsif bacterias == generar_habitaciones
      puts 'BACTERIA DESAPARECE'
      break
    elsif bacterias == nuevas_bacterias
      puts 'BACTERIA SE MANTIENE'
      break
    end
    bacterias = nuevas_bacterias
    i+=1
    puts " "
  end
end

def ejecutar_infeccionTotal(bacterias)
  suma = 0
  i = 1
  muertos = []
  loop do
    imprimir_hospital(bacterias)
    c = calcula_muertes_por_ciclo(bacterias,muertos)
    d = count_el(bacterias)
    suma += (c*10)
    puts "Ciclo " + i.to_s+"\t \t Muertos en el ciclo : " + (c*10).to_s
    puts "Habitaciones Infectadas : " + d.to_s
    puts "Porcentaje de Infección :  " + (d*1.5625).to_s+"%"
    puts "Muertos Totales :  " + suma.to_s
    nuevas_bacterias = revisar_infeccion_del_hospital(bacterias)
    i+=1
    if bacterias == generar_habitaciones('X')
      puts 'BACTERIA INFECTA TODO EL HOSPITAL'
      break
    elsif bacterias == generar_habitaciones
      puts 'BACTERIA DESAPARECE'
      break
    elsif bacterias == nuevas_bacterias
      puts 'BACTERIA SE MANTIENE'
      break
    end
    bacterias = nuevas_bacterias
    puts " "
  end
end

def ejecutar_desinfeccionTotal(bacterias)
  suma = 0
  i = 1
  muertos = []
  d = count_el(bacterias)
  loop do
    imprimir_hospital(bacterias)
    c = calcula_muertes_por_ciclo(bacterias,muertos)
    suma += (c*10)
    puts "Ciclo " + i.to_s+"\t \t Muertos en el ciclo : " + (c*10).to_s
    puts "Habitaciones Infectadas : " + d.to_s
    puts "Porcentaje de Infección :  " + (d*1.5625).to_s+"%"
    puts "Muertos Totales :  " + suma.to_s
    nuevas_bacterias = revisar_infeccion_del_hospital(bacterias)
    i+=1
    if bacterias == generar_habitaciones
      puts 'BACTERIA DESAPARECE'
      break
    elsif bacterias == nuevas_bacterias
      puts 'BACTERIA SE MANTIENE'
      break
    end
    bacterias = nuevas_bacterias
    puts " "
  end
end


def calcula_muertes_por_ciclo(bacterias,muertos_anteriores)
  posiciones_actuales = []
  bacterias.each_char.with_index do |caracter, idx|
    posiciones_actuales << idx if caracter == 'X'
  end
  muertos_anteriores.each do |posicion|
    bacterias[posicion] = ' '
  end
  c = count_el(bacterias)
  bacterias.each_char.with_index do |caracter, idx|
    muertos_anteriores << idx if caracter == 'X'
  end
  posiciones_actuales.each do |posi|
    bacterias[posi] = 'X'
  end
  return c
end



def infecta_habitacion(elementos_hnos=0)
  if elementos_hnos == 0 || elementos_hnos == 1
    ' '
  elsif elementos_hnos == 2 || elementos_hnos == 3
    'X'
  else
    ' '
  end
end

def desinfecta_habitacion(elementos_hnos=0)
  if elementos_hnos == 0 || elementos_hnos == 1 || elementos_hnos == 2
    ' '
  elsif elementos_hnos == 3
    'X'
  else
    ' '
  end
end

def decidir_infeccion_de_bacteria(elementos_hnos=0, es_hab_infectada=false)
  if es_hab_infectada
    infecta_habitacion(elementos_hnos)
  else
    desinfecta_habitacion(elementos_hnos)
  end
end

def buscar_hermanos_de_fila(col_actual, el, habs_infectadas)
  filas = (col_actual+1) * obtener_habitaciones(habs_infectadas)-1
  rango = obtener_habitaciones(habs_infectadas)
  elemento_actual= (rango*col_actual)+el
  izquierda = elemento_actual
  derecha = elemento_actual+1
  if elemento_actual-1 >= rango*col_actual
    izquierda = elemento_actual-1
  elsif izquierda == rango*col_actual
    izquierda = rango*col_actual
  end
  if elemento_actual+1 <= filas
    derecha = elemento_actual+1
  elsif derecha-1 == elemento_actual
    derecha = elemento_actual
  end
  es_infectado = false
  if habs_infectadas[elemento_actual] == 'X'
    es_infectado = true
  end

  return izquierda, derecha, es_infectado
end

def count_el(string, substring='X')
  string.scan(/(?=#{substring})/).count
end

def revisar_infeccion_del_hospital(habs_infectadas='')
  tamanio = obtener_habitaciones(habs_infectadas)
  nuevo_habitaciones_infectadas =''
  idx=0
  while true
    break if idx > tamanio-1
    col_actual = tamanio*idx
    (0..7).each do |el|
      hno = ''
      coordenadas_actual = buscar_hermanos_de_fila(idx, el, habs_infectadas)
      hno << habs_infectadas[coordenadas_actual[0]..coordenadas_actual[1]]
      if col_actual-1>0
        coordenada_anterior = [coordenadas_actual[0]-8, coordenadas_actual[1]-8]
        hno << habs_infectadas[coordenada_anterior[0]..coordenada_anterior[1]]
      end
      if col_actual+1 <(tamanio*tamanio)-tamanio
        coordenada_posterior = [coordenadas_actual[0]+8, coordenadas_actual[1]+8]
        hno << habs_infectadas[coordenada_posterior[0]..coordenada_posterior[1]]
      end
      cantidad_hermanos = count_el(hno)
      if coordenadas_actual[2]
        cantidad_hermanos-=1
      end
      cantidad_hermanos
      nuevo_habitaciones_infectadas << decidir_infeccion_de_bacteria(cantidad_hermanos,coordenadas_actual[2])
    end
    idx += 1
  end
  nuevo_habitaciones_infectadas
end

def imprimir_hospital(matrix)
  print " MEMORIAL HOSPITAL \n "
  print " "+"1"+" "+"2"+" "+"3"+" "+"4"+" "+"5"+" "+"6"+" "+"7"+" "+"8"+" "+"\n"
  print "1 "+matrix[0]+" "+matrix[1]+" "+matrix[2]+" "+matrix[3]+" "+matrix[4]+" "+matrix[5]+" "+matrix[6]+" "+matrix[7]+"\n"
  print "2 "+matrix[8]+" "+matrix[9]+" "+matrix[10]+" "+matrix[11]+" "+matrix[12]+" "+matrix[13]+" "+matrix[14]+" "+matrix[15]+"\n"
  print "3 "+matrix[16]+" "+matrix[17]+" "+matrix[18]+" "+matrix[19]+" "+matrix[20]+" "+matrix[21]+" "+matrix[22]+" "+matrix[23]+"\n"
  print "4 "+matrix[24]+" "+matrix[25]+" "+matrix[26]+" "+matrix[27]+" "+matrix[28]+" "+matrix[29]+" "+matrix[30]+" "+matrix[31]+"\n"
  print "5 "+matrix[32]+" "+matrix[33]+" "+matrix[34]+" "+matrix[35]+" "+matrix[36]+" "+matrix[37]+" "+matrix[38]+" "+matrix[39]+"\n"
  print "6 "+matrix[40]+" "+matrix[41]+" "+matrix[42]+" "+matrix[43]+" "+matrix[44]+" "+matrix[45]+" "+matrix[46]+" "+matrix[47]+"\n"
  print "7 "+matrix[48]+" "+matrix[49]+" "+matrix[50]+" "+matrix[51]+" "+matrix[52]+" "+matrix[53]+" "+matrix[54]+" "+matrix[55]+"\n"
  print "8 "+matrix[56]+" "+matrix[57]+" "+matrix[58]+" "+matrix[59]+" "+matrix[60]+" "+matrix[61]+" "+matrix[62]+" "+matrix[63]+"\n"
end


habitaciones = generar_habitaciones
imprimir_hospital(habitaciones)
bacterias = obtener_bacterias(habitaciones)
puts "--------------------------------------------------"
print "Desea realizar simulación de infección, Ejemplo: Si,No => "
condicion = gets.chomp.to_s
if condicion.upcase == "SI"
  print "Ingrese número de ciclos a simular => "
  cantidad_ciclos = gets.chomp.to_i
  puts " "
  ejecutar_ciclos(cantidad_ciclos, bacterias)
else
  print "Desea simular la desaparición de la bacteria, Ejemplo: Si, No=> "
  condicionInfeccion  = gets.chomp.to_s
  if condicionInfeccion.upcase == "SI"
    ejecutar_infeccionTotal(bacterias)
  else
    ejecutar_desinfeccionTotal(bacterias)
  end
end
