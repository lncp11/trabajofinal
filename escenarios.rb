
TAMANIO_DE_FILA = 8
TAMANIO_DEL_HOSPITAL = TAMANIO_DE_FILA * TAMANIO_DE_FILA
HABITACION_INFECTADA = 'X'.freeze
HABITACION_LIMPIA = ' '.freeze
MUERTES_POR_HABITACION = 10
FACTOR_PORCENTAJE = 1.5625

def decidir_infeccion_de_bacteria1(elementos_hnos=0, es_hab_infectada=false)
  if es_hab_infectada
    infecta_habitacion1(elementos_hnos)
  else
    desinfecta_habitacion1(elementos_hnos)
  end
end



def infecta_habitacion1(elementos_hnos=0)
  if elementos_hnos == 0 || elementos_hnos == 1
    ' '
  elsif elementos_hnos == 2 || elementos_hnos == 3
    'X'
  else
    ' '
  end
end

def desinfecta_habitacion1(elementos_hnos=0)
  if elementos_hnos == 0 || elementos_hnos == 1 || elementos_hnos == 2
    ' '
  elsif elementos_hnos == 3
    'X'
  else
    ' '
  end
end


def decidir_infeccion2(habitaciones_infectadas = 0, es_habitacion_infectada = false)
  hi = habitaciones_infectadas
  es_habitacion_infectada ? infecta_habitacion2(hi) : desinfecta_habitacion2(hi)
end

def infecta_habitacion2(habitaciones_infectadas = 0)
  hi = habitaciones_infectadas
  if [0, 1].include?(hi)
    HABITACION_LIMPIA
  elsif [2, 3].include?(hi)
    HABITACION_INFECTADA
  else
    HABITACION_LIMPIA
  end
end

def desinfecta_habitacion2(habitaciones_infectadas = 0)
  hi = habitaciones_infectadas
  if [0, 1, 2].include?(hi)
    HABITACION_LIMPIA
  elsif hi == 3
    HABITACION_INFECTADA
  else
    HABITACION_LIMPIA
  end
end


# var =  8
# cond = false
#
# puts decidir_infeccion_de_bacteria1(var,cond)
# puts decidir_infeccion2(var,cond)
# puts ' '





def buscar_hermanos_de_fila1(col_actual, el, habs_infectadas)
  filas = (col_actual+1) * obtener_habitaciones1(habs_infectadas)-1
  rango = obtener_habitaciones1(habs_infectadas)
  elemento_actual= (rango*col_actual)+el
  izquierda = elemento_actual
  derecha = elemento_actual+1

  if elemento_actual+1 <= filas
    derecha = elemento_actual+1
  elsif derecha-1 == elemento_actual
    derecha = elemento_actual
  end


  if elemento_actual-1 >= rango*col_actual
    izquierda = elemento_actual-1
  elsif izquierda == rango*col_actual
    izquierda = rango*col_actual
  end






  es_infectado = false
  if habs_infectadas[elemento_actual] == 'X'
    es_infectado = true
  end

  return izquierda, derecha, es_infectado
end






def obtener_habitaciones1(habitaciones)
  Math.sqrt(habitaciones.length).round
end

def count_el1(string, substring='X')
  string.scan(/(?=#{substring})/).count
end





def buscar_hermanos_de_habitacion2(eje_x, eje_y, habitaciones)
  indice_habitacion_actual = (TAMANIO_DE_FILA * eje_x) + eje_y
  habitacion_actual = habitaciones[indice_habitacion_actual]
  limite_izquierda = TAMANIO_DE_FILA * eje_x
  limite_derecha = (8 * eje_x) + eje_y

  # filas = (col_actual+1) * obtener_habitaciones1(habs_infectadas)-1
  # rango = obtener_habitaciones1(habs_infectadas)
  # elemento_actual= (rango*col_actual)+el
  # izquierda = elemento_actual
  # derecha = elemento_actual+1


  [
    buscar_habitaciones_anteriores2(indice_habitacion_actual, limite_izquierda),
    buscar_habitaciones_posteriores2(indice_habitacion_actual + 1, limite_derecha),
    es_habitacion_infectada2(habitacion_actual)
  ]
end
def buscar_habitaciones_posteriores2(indice_habitacion_actual, limite_derecha)
  habitacion_posterior = indice_habitacion_actual
  hp = habitacion_posterior
  if indice_habitacion_actual + 1 <= limite_derecha
    hp = indice_habitacion_actual + 1
  elsif hp - 1 == indice_habitacion_actual
    hp = indice_habitacion_actual
  end
  hp
end





def buscar_habitaciones_anteriores2(indice_habitacion_actual, limite_izquierda)
  habitacion_anterior = indice_habitacion_actual
  ha = habitacion_anterior
  if indice_habitacion_actual - 1 >= limite_izquierda
    ha = indice_habitacion_actual - 1
  elsif ha == limite_izquierda
    ha = limite_izquierda
  end
  ha
end



def es_habitacion_infectada2(habitacion_actual)
  habitacion_actual == HABITACION_INFECTADA ? true : false
end

def obtener_habitaciones_actual2(coordenadas, habitaciones)
  habitaciones[coordenadas[0]...coordenadas[1]]
end



def obtener_habitaciones_superior2(coordenadas, habitaciones)
  habitaciones_izquierda = coordenadas[0] - TAMANIO_DE_FILA
  habitaciones_derecha = coordenadas[1] - TAMANIO_DE_FILA
  habitaciones[habitaciones_izquierda...habitaciones_derecha]
end

def obtener_habitaciones_inferior2(coordenadas, habitaciones)
  habitaciones_izquierda = coordenadas[0] + TAMANIO_DE_FILA
  habitaciones_derecha = coordenadas[1] + TAMANIO_DE_FILA
  habitaciones[habitaciones_izquierda...habitaciones_derecha]
end
def contar_habitaciones_por_cadena2(string, substring = HABITACION_INFECTADA)
  string.scan(/(?=#{substring})/).count
end


def generar_habitaciones2(caracter = ' ')
  caracter * TAMANIO_DEL_HOSPITAL
end



def revisar_infeccion_del_hospital1(habs_infectadas='')
  tamanio = obtener_habitaciones1(habs_infectadas)
  nuevo_habitaciones_infectadas =''
  idx=0
  while true
    break if idx > tamanio-1
    col_actual = tamanio*idx
    (0..7).each do |el|
      hno = ''
      coordenadas_actual = buscar_hermanos_de_fila1(idx, el, habs_infectadas)
      hno << habs_infectadas[coordenadas_actual[0]..coordenadas_actual[1]]
      if col_actual-1>0
        coordenada_anterior = [coordenadas_actual[0]-8, coordenadas_actual[1]-8]
        hno << habs_infectadas[coordenada_anterior[0]..coordenada_anterior[1]]
      end
      if col_actual+1 <(tamanio*tamanio)-tamanio
        coordenada_posterior = [coordenadas_actual[0]+8, coordenadas_actual[1]+8]
        hno << habs_infectadas[coordenada_posterior[0]..coordenada_posterior[1]]
      end
      cantidad_hermanos = count_el1(hno)
      if coordenadas_actual[2]
        cantidad_hermanos-=1
      end
      # cantidad_hermanos
      nuevo_habitaciones_infectadas << decidir_infeccion_de_bacteria1(cantidad_hermanos,coordenadas_actual[2])
    end
    idx += 1
  end
  nuevo_habitaciones_infectadas
end

def revisar_infeccion_del_hospital2(habitaciones_infectadas = generar_habitaciones)
  # habitaciones_infectadas = ''
  tamanio = obtener_habitaciones1(habitaciones_infectadas)
  nuevo_habitaciones_infectadas =''
  eje_x=0

  while true
    break if eje_x > tamanio - 1
    columna_actual = tamanio * eje_x
    (0..7).each do |eje_y|
      nuevas_habitaciones_infectadas = ''
      coordenadas_de_habitaciones_hermanas = buscar_hermanos_de_fila1(eje_x, eje_y, habitaciones_infectadas)
      nuevas_habitaciones_infectadas << habitaciones_infectadas[coordenadas_de_habitaciones_hermanas[0]..coordenadas_de_habitaciones_hermanas[1]]
      if columna_actual-1>0
        coordenada_anterior = [coordenadas_de_habitaciones_hermanas[0]-8, coordenadas_de_habitaciones_hermanas[1]-8]
        nuevas_habitaciones_infectadas << habitaciones_infectadas[coordenada_anterior[0]..coordenada_anterior[1]]
      end
      if columna_actual+1 <(tamanio*tamanio)-tamanio
        coordenada_posterior = [coordenadas_de_habitaciones_hermanas[0]+8, coordenadas_de_habitaciones_hermanas[1]+8]
        nuevas_habitaciones_infectadas << habitaciones_infectadas[coordenada_posterior[0]..coordenada_posterior[1]]
      end
      cantidad_hermanos = count_el1(nuevas_habitaciones_infectadas)
      if coordenadas_de_habitaciones_hermanas[2]
        cantidad_hermanos -= 1
      end
      # cantidad_hermanos
      nuevo_habitaciones_infectadas << decidir_infeccion_de_bacteria1(cantidad_hermanos,coordenadas_de_habitaciones_hermanas[2])
    end
    eje_x += 1
  end
  nuevo_habitaciones_infectadas
end

#
# var_hospital='XX     XX      X                                XX      X      X'
#
#
# puts revisar_infeccion_del_hospital1(var_hospital)
# puts revisar_infeccion_del_hospital2(var_hospital)
# puts ''



var_hospital='XX     XX      X                                XX      X      X'
puts buscar_hermanos_de_fila1(1,1, var_hospital)
puts buscar_hermanos_de_habitacion2(1,1,var_hospital)
